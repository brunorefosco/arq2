package arq.segundo.intento;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author martinbravo
 */
public class FIFO extends Politicas {

    int estado = 0; //1= cargado, 2= listo, 3= ejecutando, 4= bloqueado, 5 terminado.
    Proceso p;
    Scanner s = new Scanner(System.in);
    int entrada;
    ArrayList momento = new ArrayList();

    public void Dibujar() {

        do {
            Carga();
            Listo();
            Ejecucion(0, 0);
            Bloqueo();
            Termina();
        } while (estado != 5);
    }

    public void Carga() {

        if (estado == 0) {
            estado = 1;
            momento.add("cargado");

        }
    }

    public void Listo() {

        if (estado == 1) {
            estado = 2;
            momento.add("listo");

        }

    }

    public void Ejecucion(int je, int aejecutar) {

        if (estado == 2) {
            estado = 3;
            for (int i = 0; i < je; i++) {
                //while (aejecutar > 0) { MUESTRA TODAS JUNTAS
                momento.add("ejecutando");
                //aejecutar--;        ^^^^^^^^^^^^^^ESO
            }

            Ejecucion(je, aejecutar);
        } else {
            Bloqueo();
        }

    }

    public void Bloqueo() {
//si tiene una rafaga mas, o tiempo por ejecutar
        if (estado == 3) {
            //tiempo e/s cumplido
            estado = 2;
            momento.add("Bloqueado");

        }

    }

    public void Termina() {

        if (estado == 3)//&& ejecucion terminada )
        {
            estado = 5;
            momento.add("terminado");

        }

    }

    @Override
    public void Ejecucion() {

    }

    @Override
    public void Desalojo() {

    }

}
